package ru.t1.azarin.tm.api.repository;

import ru.t1.azarin.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}